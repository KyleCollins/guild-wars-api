import urllib
import json
import LocalDB
from JsonURL import JsonFile

URLId = "19684"
URLSpecificId = "https://api.guildwars2.com/v2/commerce/listings/" + URLId
JsonURL = JsonFile(URLSpecificId)

def AddToListings(jsonFile):
	for json in jsonFile:
		InsertListings(json)
		print(str(json))

	LocalDB.CommitChanges()
	CloseConnection()

def IterateBuyItems(jsonFile):
	BuyData = jsonFile['buys']
	for bd in BuyData:
		print('Listings: ' + str(bd['listings']))
		print('Unit Price: ' + str(bd['unit_price']))
		print('Quantity: ' + str(bd['quantity']))
		print('')

def IterateSellItems(jsonFile):
	sellData = jsonFile['sells']
	for bd in sellData:
		print('Listings: ' + str(bd['listings']))
		print('Unit Price: ' + str(bd['unit_price']))
		print('Quantity: ' + str(bd['quantity']))
		print('')

	LocalDB.CommitChanges()
	LocalDB.CloseConnection()

def PrintIndividualID():
	Iterate(JsonURL)

def Iterate(jsonFile):
	for json in jsonFile:
		print(str(json))

		
IterateSellItems(JsonURL)


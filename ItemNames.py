import urllib
import json
from JsonURL import JsonFile

#URLSpecificId = "https://api.guildwars2.com/v2/commerce/listings/19684"
ItemListings = "https://api.guildwars2.com/v2/commerce/listings/"
ItemName = "https://api.guildwars2.com/v2/items/"
JsonItemListings = JsonFile(ItemListings)
JsonItemName = JsonFile(ItemName)

def IterateItemListings():
	for item in JsonItemListings:
		 print(str(item) + " " + str(GetItemName(item)))

#Get Item Names
def GetItemName(itemNumber):
	return GetItemNameJson(itemNumber)['name']

def GetItemNameJson(itemNumber):
	return JsonFile(GetItemNameURL(itemNumber))

def GetItemNameURL(itemNumber):
	return ItemName + str(itemNumber)


import urllib
import json
import LocalDB.MasterControl
from JsonURL import JsonFile

URLId = "19684"
URLSpecificId = "https://api.guildwars2.com/v2/commerce/listings/" + URLId
JsonURL = JsonFile(URLSpecificId)

def PrintSellItems(jsonFile):
	sellData = jsonFile['sells']
	for bd in sellData:
		print('Listings: ' + str(bd['listings']))
		print('Unit Price: ' + str(bd['unit_price']))
		print('Quantity: ' + str(bd['quantity']))
		print('')
		
file = 'GW2API.sqlite'
LocalDB.MasterControl.Main(file);
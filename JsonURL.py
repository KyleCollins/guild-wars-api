import urllib, json

def JsonFile(url):
	response = urllib.urlopen(url)
	jsonFile = json.loads(response.read())
	return jsonFile
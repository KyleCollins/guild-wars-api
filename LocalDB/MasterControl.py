import sqlite3
import Select, Delete

def InitializeDB(sqlite_file):
	connection = _Connection(sqlite_file)
	cursor = _Cursor(connection)
	return cursor

def SelectQuery(cursor, query):
	Select.SelectQuery(cursor, query)

def DeleteQuery(cursor, query):
	Delete.DeleteQuery(cursor, query)

def CommitChanges(connection):
	connection.commit()

def CloseConnection(connection):
	connection.close()

def _Connection(sqlite_file):
	connection = sqlite3.connect(sqlite_file)
	return connection

def _Cursor(connection):
	cursor = connection.cursor()
	return cursor

def Main(file):
	cursor = InitializeDB(file)
	SelectQuery(cursor, 'SELECT * FROM SellCommerceListings')